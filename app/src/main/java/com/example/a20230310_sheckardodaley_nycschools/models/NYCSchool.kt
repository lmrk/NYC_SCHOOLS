package com.example.a20230310_sheckardodaley_nycschools.models

import com.google.gson.annotations.SerializedName

data class NYCSchool(
    @SerializedName("dbn")
    val schoolCode: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("building_code")
    val buildingCode: String,
    @SerializedName("location")
    val address: String,
    @SerializedName("phone_number")
    val phoneNumber: String,
    @SerializedName("school_email")
    val email: String,
    val website : String,
    @SerializedName("total_students")
    val totalStudents: String
)
