package com.example.a20230310_sheckardodaley_nycschools.repository

import com.example.a20230310_sheckardodaley_nycschools.utilities.ResponseStates
import kotlinx.coroutines.flow.Flow

interface SchoolDataRepository {
    //repository methods that will be overridden as needed
    suspend fun getNYCSchoolsFromApi() : Flow<ResponseStates>
    suspend fun getSATScoresFromApi(code : String) : Flow<ResponseStates>
}