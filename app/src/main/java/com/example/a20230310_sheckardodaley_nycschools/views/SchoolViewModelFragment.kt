package com.example.a20230310_sheckardodaley_nycschools.views

import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.a20230310_sheckardodaley_nycschools.dagger.DaggerSchoolsViewModelComponent
import com.example.a20230310_sheckardodaley_nycschools.viewmodel.SchoolsViewModel

open class SchoolViewModelFragment : Fragment() {

    protected val viewModel: SchoolsViewModel by activityViewModels() {
        val component = DaggerSchoolsViewModelComponent.create()
       val viewModelFactory = component.getViewModelFactory()
        viewModelFactory
    }
}