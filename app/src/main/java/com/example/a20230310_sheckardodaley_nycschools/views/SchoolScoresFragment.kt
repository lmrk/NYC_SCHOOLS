package com.example.a20230310_sheckardodaley_nycschools.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.a20230310_sheckardodaley_nycschools.R
import com.example.a20230310_sheckardodaley_nycschools.databinding.SchoolScoreFragmentLayoutBinding
import com.example.a20230310_sheckardodaley_nycschools.models.SATScore
import com.example.a20230310_sheckardodaley_nycschools.utilities.ResponseStates

class SchoolScoresFragment : SchoolViewModelFragment() {

    private var scoreLayoutBinding: SchoolScoreFragmentLayoutBinding? = null
    private val binding get() = scoreLayoutBinding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        scoreLayoutBinding = SchoolScoreFragmentLayoutBinding.inflate(layoutInflater)
        initObserver()
        return binding.root

    }

    private fun initObserver() {
        viewModel.scoreDataForSchoolLiveData.observe(viewLifecycleOwner) { state ->
            val school = viewModel.currSchool
            when (state) {
                is ResponseStates.OnResponseSuccess<*> -> {
                    val score: SATScore? = (state.response as List<SATScore>).firstOrNull()
                    if (score == null) {
                        binding.apply {
                            pbScoreLoading.visibility = View.GONE
                            emptySatScoresError.visibility = View.VISIBLE
                            emptySatScoresError.setText(R.string.blank_avg_score)
                        }
                    } else {
                        binding.apply {
                            pbScoreLoading.visibility = View.GONE
                            emptySatScoresError.visibility = View.GONE
                            tvScoreTakers.text = resources.getString(R.string.sat_takers, score.totalTestTakers)
                            tvScoreMath.text = resources.getString(R.string.avg_math_score, score.mathAvgScore)
                            tvScoreReading.text = resources.getString(R.string.avg_crit_reading_score, score.criticalReadingAvgScore)
                            tvScoreWriting.text = resources.getString(R.string.avg_writing_score, score.writingAvgScore)
                            schoolScores.visibility = View.VISIBLE
                        }
                    }
                    binding.apply {
                        tvScoreSchoolName.text = school?.schoolName
                        tvTotalStudents.text = resources.getString(R.string.total_students, school?.totalStudents)
                    }
                }
                is ResponseStates.OnResponseError -> {
                    binding.apply {
                        pbScoreLoading.visibility = View.GONE
                        emptySatScoresError.setText(R.string.error_sat_scores)
                        emptySatScoresError.visibility = View.VISIBLE
                    }
                }
                is ResponseStates.OnResponseLoading -> {
                    binding.emptySatScoresError.visibility = View.GONE
                    viewModel.fetchSATScoreForSchool(viewModel.currSchool?.schoolCode ?: "")
                }
            }
        }
    }



    override fun onDestroyView() {
        super.onDestroyView()
        scoreLayoutBinding = null
    }
}