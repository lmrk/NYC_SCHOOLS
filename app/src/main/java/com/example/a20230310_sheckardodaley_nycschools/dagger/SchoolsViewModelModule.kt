package com.example.a20230310_sheckardodaley_nycschools.dagger

import com.example.a20230310_sheckardodaley_nycschools.network.SchoolApiService
import com.example.a20230310_sheckardodaley_nycschools.repository.SchoolDataRepositoryImpl
import com.example.a20230310_sheckardodaley_nycschools.viewmodel.SchoolsViewModel
import com.example.a20230310_sheckardodaley_nycschools.viewmodel.ViewModelFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class SchoolsViewModelModule {

    @Singleton
    @Provides
    fun provideNetworkConnection() : SchoolApiService {
        val BASE_URL = "https://data.cityofnewyork.us/resource/"
        val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(provideOkHttpClient())
            .build()
            .create(SchoolApiService::class.java)
        return retrofit
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val client = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

        return client
    }


    @Singleton
    @Provides
    fun provideSchoolRepository() : SchoolDataRepositoryImpl {
        return SchoolDataRepositoryImpl(provideNetworkConnection())
    }

    @Singleton
    @Provides
    fun provideSchoolViewModel() : SchoolsViewModel {
        return SchoolsViewModel(provideSchoolRepository())
    }

    @Provides
    fun provideViewModelFactory() : ViewModelFactory {
        return ViewModelFactory(provideSchoolRepository())
    }
}