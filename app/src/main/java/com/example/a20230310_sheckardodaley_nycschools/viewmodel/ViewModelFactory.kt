package com.example.a20230310_sheckardodaley_nycschools.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.a20230310_sheckardodaley_nycschools.repository.SchoolDataRepositoryImpl

class ViewModelFactory (private val repository: SchoolDataRepositoryImpl) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) : T {

        if (modelClass.isAssignableFrom(SchoolsViewModel::class.java)) {
            return SchoolsViewModel(repository) as T
        }
        throw java.lang.IllegalArgumentException("Unknown ViewModel class")
    }
}